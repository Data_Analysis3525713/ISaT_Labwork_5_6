import numpy as np
import pandas as pd
from sklearn import svm
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.inspection import DecisionBoundaryDisplay
import umap as umap
from sklearn.manifold import TSNE
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.ensemble import RandomForestClassifier as RF


# function loading data and getting y = dataset.target 
def prepare_data (path: str, return_type="ndArray"):
    X = np.genfromtxt(path, delimiter=",")
    y = np.array([])

    for i in X:
        y = np.append(y, i[5])
        
    X = np.delete(X, 5, 1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    
    if return_type == "ndArray": 
        return np.array(X_train), np.array(X_test), np.array(y_train), np.array(y_test)

    if return_type == "dataframe": 
        X_train = pd.DataFrame(data=X_train)
        X_test = pd.DataFrame(data=X_test)
        y_train = pd.DataFrame(data=y_train)
        y_test = pd.DataFrame(data=y_test)
        return X_train, X_test, y_train, y_test
    
    if return_type == "list":
        #X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
        return X_train.tolist(), X_test.tolist(), y_train.tolist(), y_test.tolist()

# function counting metrics: accuracy, precision, recall, F1
""" def count_metrics (TP, FP, TN, FN, Clf_type):
    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    #F1 = (2 * precision * recall) / (precision + recall)
    #accuracy = (TP + TN) / (TP + FP + TN + FN)
    print(f"------ Classifier type: {Clf_type} ------")
    print(f"Accuracy = {(TP + TN) / (TP + FP + TN + FN)}")
    print(f"Pricision = {TP / (TP + FP)}")
    print(f"Recall = {TP / (TP + FN)}")
    print(f"F1-measure = {(2 * precision * recall) / (precision + recall)}") """

def functional_metrics (y_test, y_test_pred, title):
        print(f"------- {title} -------")
        print(f"Accuracy score: {accuracy_score(y_test, y_test_pred)}")
        print(f"Precision score: {precision_score(y_test, y_test_pred, labels=y_test, average='micro')}")
        print(f"Recall score: {recall_score(y_test, y_test_pred, labels=y_test, average='macro')}")
        print(f"F1-measure score: {f1_score(y_test, y_test_pred, labels=y_test, average='macro')}")

def _vizualise (models, titles, size_1, size_2, X_train, y_train):
    fig, sub = plt.subplots(size_1, size_2, figsize=(9,8))
    plt.subplots_adjust(wspace=1.2, hspace=1.2)

    X0, X1 = X_train[:, 0], X_train[:, 1]

    for clf, title, ax in zip(models, titles, sub.flatten()):
        disp = DecisionBoundaryDisplay.from_estimator(
            clf,
            X_train,
            response_method="predict",
            cmap=plt.cm.coolwarm,
            alpha=0.8,
            ax=ax,
            xlabel="X",
            ylabel="Y",
        )
        ax.scatter(X0, X1, c=y_train, cmap=plt.cm.coolwarm, s=20, edgecolors="k")
        ax.set_xticks(())
        ax.set_yticks(())
        ax.set_title(title)

    plt.show()

def view_SVM(X_train, X_test, y_train, y_test, mode="viz"):
    models = (
        svm.SVC(kernel="linear", C=1, decision_function_shape="ovr"),
        svm.SVC(kernel="linear", C=50, decision_function_shape="ovr"),
        svm.SVC(kernel="linear", C=100, decision_function_shape="ovr"),
        svm.SVC(kernel="linear", C=500, decision_function_shape="ovr"),

        svm.SVC(kernel="rbf", C=1, decision_function_shape="ovr"),
        svm.SVC(kernel="rbf", C=50, decision_function_shape="ovr"),
        svm.SVC(kernel="rbf", C=100, decision_function_shape="ovr"),
        svm.SVC(kernel="rbf", C=500, decision_function_shape="ovr"),

        svm.SVC(kernel="poly", C=1, decision_function_shape="ovr"),
        svm.SVC(kernel="poly", C=50, decision_function_shape="ovr"),
        svm.SVC(kernel="poly", C=100, decision_function_shape="ovr"),
        svm.SVC(kernel="poly", C=500, decision_function_shape="ovr"),

        svm.SVC(kernel="sigmoid", C=1, decision_function_shape="ovr"),
        svm.SVC(kernel="sigmoid", C=50, decision_function_shape="ovr"),
        svm.SVC(kernel="sigmoid", C=100, decision_function_shape="ovr"),
        svm.SVC(kernel="sigmoid", C=500, decision_function_shape="ovr"),
    )
    models = (clf.fit(X_train, y_train) for clf in models)
    
    #models_repeat = (clf.fit(X_train, y_train) for clf in models)!!!!!!!!!!!!!!!!!

    titles = (
        "SVC with linear kernel \nand C = 1",
        "SVC with linear kernel \nand C = 50",
        "SVC with linear kernel \nand C = 100",
        "SVC with linear kernel \nand C = 500",
        "SVC with rbf kernel \nand C = 1",
        "SVC with rbf kernel \nand C = 50",
        "SVC with rbf kernel \nand C = 100",
        "SVC with rbf kernel \nand C = 500",
        "SVC with \npolynomial kernel \nand C = 1",
        "SVC with \npolynomial kernel \nand C = 50",
        "SVC with \npolynomial kernel \nand C = 100",
        "SVC with \npolynomial kernel \nand C = 500",
        "SVC with \nsigmoid kernel \nand C = 1",
        "SVC with \nsigmoid kernel \nand C = 50",
        "SVC with \nsigmoid kernel \nand C = 100",
        "SVC with \nsigmoid kernel \nand C = 500",
    )

    titles_for_metrics = (
        "SVC with linear kernel and C = 1",
        "SVC with linear kernel and C = 50",
        "SVC with linear kernel and C = 100",
        "SVC with linear kernel and C = 500",
        "SVC with rbf kernel and C = 1",
        "SVC with rbf kernel and C = 50",
        "SVC with rbf kernel and C = 100",
        "SVC with rbf kernel and C = 500",
        "SVC with polynomial kernel and C = 1",
        "SVC with polynomial kernel and C = 50",
        "SVC with polynomial kernel and C = 100",
        "SVC with polynomial kernel and C = 500",
        "SVC with sigmoid kernel and C = 1",
        "SVC with sigmoid kernel and C = 50",
        "SVC with sigmoid kernel and C = 100",
        "SVC with sigmoid kernel and C = 500",
    )

    if mode == "viz":
        _vizualise(models, titles, 4, 4, X_train, y_train)
    elif mode == "met":
        print("----------------------- SVM -----------------------")
        for model, title in zip(models, titles_for_metrics):
            print(f"Количество опорных векторов: {model.n_support_}")
            y_test_predicted = model.predict(X_test)
            functional_metrics(y_test, y_test_predicted, title)

def view_KNN(X_train, X_test, y_train, y_test, mode="viz"):
    models = (
        KNN(n_neighbors=1, metric='minkowski', weights='uniform'),
        KNN(n_neighbors=5, metric='minkowski', weights='uniform'),
        KNN(n_neighbors=7, metric='minkowski', weights='uniform'),

        KNN(n_neighbors=5, metric='euclidean', weights='uniform'),
        KNN(n_neighbors=5, metric='manhattan', weights='uniform'),
        KNN(n_neighbors=5, metric='cosine', weights='uniform'),

        KNN(n_neighbors=5, metric='euclidean', weights='distance'),
        KNN(n_neighbors=5, metric='manhattan', weights='distance'),
        KNN(n_neighbors=5, metric='cosine', weights='distance'),
    )
    models = (clf.fit(X_train, y_train) for clf in models)
    
    #models_repeat = (clf.fit(X_train, y_train) for clf in models)!!!!!!!!!!!!!!!!!

    titles = (
        "kNN with 1 neighbor,\nminkowski metric \nand uniform voting",
        "kNN with 5 neighbors,\nminkowski metric \nand uniform voting",
        "kNN with 7 neighbors,\nminkowski metric \nand uniform voting",

        "kNN with 5 neighbors,\neuclidean metric \nand uniform voting",
        "kNN with 5 neighbors,\nmanhattan metric \nand uniform voting",
        "kNN with 5 neighbors,\ncosine metric \nand uniform voting",

        "kNN with 5 neighbors,\neuclidean metric \nand distance voting",
        "kNN with 5 neighbors,\nmanhattan metric \nand distance voting",
        "kNN with 5 neighbors,\ncosine metric \nand uniform voting",
    )

    titles_for_metrics = (
        "kNN with 1 neighbor, minkowski metric and uniform voting",
        "kNN with 5 neighbor, minkowski metric and uniform voting",
        "kNN with 7 neighbor, minkowski metric and uniform voting",

        "kNN with 5 neighbor, euclidean metric and uniform voting",
        "kNN with 5 neighbor, manhattan metric and uniform voting",
        "kNN with 5 neighbor, cosine metric and uniform voting",

        "kNN with 5 neighbors, euclidean metric and distance voting",
        "kNN with 5 neighbors, manhattan metric and distance voting",
        "kNN with 5 neighbors, cosine metric and uniform voting",
    )

    if mode == "viz":
        _vizualise(models, titles, 3, 3, X_train, y_train)
    elif mode == "met":
        print("----------------------- kNN -----------------------")
        for model, title in zip(models, titles_for_metrics):
            y_test_predicted = model.predict(X_test)
            functional_metrics(y_test, y_test_predicted, title)

def view_RF(X_train, X_test, y_train, y_test, mode="viz"):
    models = (
        RF(max_depth=5, n_estimators=3, max_features=1),
        RF(max_depth=5, n_estimators=10, max_features=1),
        RF(max_depth=5, n_estimators=20, max_features=1),

        RF(max_depth=3, n_estimators=10, max_features=1),
        RF(max_depth=5, n_estimators=10, max_features=1),
        RF(max_depth=10, n_estimators=10, max_features=1),

        RF(max_depth=5, n_estimators=10, max_features=1),
        RF(max_depth=5, n_estimators=10, max_features=3),
        RF(max_depth=5, n_estimators=10, max_features=5),
    )
    models = (clf.fit(X_train, y_train) for clf in models)
    
    #models_repeat = (clf.fit(X_train, y_train) for clf in models)!!!!!!!!!!!!!!!!!

    titles = (
        "RF with 3 estimators,\n1 max feature \nand max_depth=5",
        "RF with 10 estimators,\n1 max feature \nand max_depth=5",
        "RF with 20 estimators,\n1 max feature \nand max_depth=5",

        "RF with 10 estimators,\n1 max feature \nand max_depth=3",
        "RF with 10 estimators,\n1 max feature \nand max_depth=5",
        "RF with 10 estimators,\n1 max feature \nand max_depth=10",

        "RF with 10 estimators,\n1 max feature \nand max_depth=5",
        "RF with 10 estimators,\n3 max features \nand max_depth=5",
        "RF with 10 estimators,\n5 max features \nand max_depth=5",
    )

    titles_for_metrics = (
        "RF with 3 estimators, 1 max feature and max_depth=5",
        "RF with 10 estimators, 1 max feature and max_depth=5",
        "RF with 20 estimators, 1 max feature and max_depth=5",

        "RF with 10 estimators, 1 max feature and max_depth=3",
        "RF with 10 estimators, 1 max feature and max_depth=5",
        "RF with 10 estimators, 1 max feature and max_depth=10",

        "RF with 10 estimators, 1 max feature and max_depth=5",
        "RF with 10 estimators, 3 max features and max_depth=5",
        "RF with 10 estimators, 5 max features and max_depth=5",
    )

    if mode == "viz":
        _vizualise(models, titles, 3, 3, X_train, y_train)
    elif mode == "met":
        print("----------------------- RF -----------------------")
        for model, title in zip(models, titles_for_metrics):
            y_test_predicted = model.predict(X_test)
            functional_metrics(y_test, y_test_predicted, title)


# ------------------------------ Here comes the main code ------------------------------
X_train, X_test, y_train, y_test = prepare_data("./hayes+roth/hayes-roth.data")
X_train_umap = umap.UMAP(n_components=2, n_neighbors=2).fit_transform(X_train)
X_test_umap = umap.UMAP(n_components=2, n_neighbors=2).fit_transform(X_test)
X_train_tsne = TSNE(n_components=2).fit_transform(X_train, y_train)
X_test_tsne = TSNE(n_components=2).fit_transform(X_test, y_test)

# vizualising
view_SVM(X_train_umap, X_test_umap, y_train, y_test, mode="viz")
""" view_SVM(X_train_tsne, X_test_tsne, y_train, y_test, mode="viz")
view_KNN(X_train_umap, X_test_umap, y_train, y_test, mode="viz")
view_KNN(X_train_tsne, X_test_tsne, y_train, y_test, mode="viz")
view_RF(X_train_umap, X_test_umap, y_train, y_test, mode="viz")
view_RF(X_train_tsne, X_test_tsne, y_train, y_test, mode="viz") """


# counting metrics
""" view_SVM(X_train_umap, X_test_umap, y_train, y_test, mode="met")
view_SVM(X_train_tsne, X_test_tsne, y_train, y_test, mode="met")
view_KNN(X_train_umap, X_test_umap, y_train, y_test, mode="met")
view_KNN(X_train_tsne, X_test_tsne, y_train, y_test, mode="met")
view_RF(X_train_umap, X_test_umap, y_train, y_test, mode="met")
view_RF(X_train_tsne, X_test_tsne, y_train, y_test, mode="met") """